const express = require('express')
const cors = require('cors')
const multer = require('multer')
const path = require('path')

const app =  express()

app.use(express.json())

const storage = multer.diskStorage({
	destination: path.join(__dirname, 'uploads'),
	filename: (req, file, cb) => {
		cb(null, new Date().getTime()+file.originalname)
	}
})

const upload = multer({
	storage,
	dest: path.join(__dirname, 'uploads'),
	limits: {
	  fileSize: 10*1024*1024 
	}
})

app.use(upload.single('video'))

app.use(cors())

app.use('/', require('./routes'))

app.listen(3333, () => {
	        console.log('Server on port 3333')
})
