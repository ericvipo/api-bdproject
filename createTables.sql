--secuencias
create sequence proy_seq
	increment by 10
	start with 10
	maxvalue 999999
	nocache
	nocycle;

create sequence ruta_seq
	increment by 1
	start with 1
	maxvalue 100
	nocache
	nocycle;

create sequence tramo_seq
	increment by 1
	start with 1
	maxvalue 100
	nocache
	nocycle;


-- tabla proyecto
create table proyecto (
	CodPyto number(6),
	NomPyto varchar2(100) not null,
	CostoTotal number(12,2) not null,
	Observac varchar2(500),
	constraint pk_pyto primary key (CodPyto)
);

-- tabla ruta
create table ruta (
	CodRuta number(6),
	CodPyto number(6) unique,
	CodRutaPyto varchar2(9) not null,
	DenCorto varchar2(60) not null,
	ZoneGPS number(3) not null,
	Longitud number(6,3) not null,
	constraint pk_ruta primary key(CodRuta),
	constraint fk_proy foreign key(CodPyto) references proyecto
);

-- tabla tramo
create table tramo (
	CodTramo number(3),
	CodRuta number(6),
	CodTramoPy varchar2(20) not null,
	Vigencia varchar2(10) not null,
	Duracion number(6,2) not null,
	ProgInicio varchar2(10) not null,
	LatInicio varchar2(20) not null,
	LongInicio varchar2(20) not null,
	AltInicio varchar2(20) not null,
	ProgFinal varchar2(20) not null,
	LatFin varchar2(20) not null,
	LongFin varchar2(20) not null,
	AltFin varchar2(20) not null,
	IdCriterioIni varchar2(50) not null,
	DenCorta varchar2(50) not null,
	Observac varchar2(200),
	VideoUrl varchar2(150) not null,
	VideoId varchar2(20) not null,
	constraint pk_tramo primary key(CodTramo),
	constraint fk_ruta foreign key(CodRuta) references ruta
);

-- inserts in proyecto
insert into proyecto(
	CodPyto, NomPyto, CostoTotal, Observac
)
values (
	proy_seq.NEXTVAL, 'CV21-Ancash',
	6648.23, 'Proyecto rurarl en Ancash 2020'
);

insert into proyecto(
	CodPyto, NomPyto, CostoTotal, Observac
)
values (
	proy_seq.NEXTVAL, 'YZ21-PUNO',
	7748.23, 'Proyecto rural en Puno 2020'
);

insert into proyecto(
	CodPyto, NomPyto, CostoTotal, Observac
)
values (
	proy_seq.NEXTVAL, 'RT55-Pucallpa',
	5748.23, 'Proyecto rural en Pucallpa 2020'
);

insert into proyecto(
	CodPyto, NomPyto, CostoTotal, Observac
)
values (
	proy_seq.NEXTVAL, 'XP00-Ayacucho',
	1748.23, 'Proyecto rural en Ayacucho 2020'
);

insert into proyecto(
	CodPyto, NomPyto, CostoTotal, Observac
)
values (
	proy_seq.NEXTVAL, 'CV25-Tacna',
	78482.21, 'Proyecto rural en Tacna 2020'
);

-- inserts in ruta
insert into ruta (
	CodRuta, CodPyto, CodRutaPyto, DenCorto, ZoneGPS, Longitud
)
values (
	ruta_seq.NEXTVAL, 10,
	'AN-112', 'Emp PE 16 hasta Emp PE 16A(Ov Rinconada)',
	18, 53.700
);

insert into ruta (
	CodRuta, CodPyto, CodRutaPyto, DenCorto, ZoneGPS, Longitud
)
values (
	ruta_seq.NEXTVAL, 20,
	'TA-142', 'Emp PE 16 hasta Emp PE 16A(Ov Bolognesi)',
	55, 47.500
);

insert into ruta (
	CodRuta, CodPyto, CodRutaPyto, DenCorto, ZoneGPS, Longitud
)
values (
	ruta_seq.NEXTVAL, 10,
	'AN-112', 'Emp PE 16 hasta Emp PE 16A(Ov Rinconada)',
	18, 53.700
);

insert into ruta (
	CodRuta, CodPyto, CodRutaPyto, DenCorto, ZoneGPS, Longitud
)
values (
	ruta_seq.NEXTVAL, 50,
	'AY-009', 'Emp PE 16 hasta Emp PE 16A(Ov Lucanas)',
	18, 53.700
);




-- delete row
delete
from ruta
where codruta = 7;

delete
from tramo
where codtramo = 17;

-- alterar tablas
alter table ruta
	modify CodPyto number(6) unique;

alter table tramo
	modify Duracion number(6,2);

alter table tramo
	modify VideoId varchar2(100);

select * from proyecto;
select * from ruta;
select * from tramo;
select * from tab;




select * from tramo
where codruta = 9;

