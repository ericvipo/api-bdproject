const oracledb = require('oracledb')
const cloudinary = require('cloudinary')

//configuracion
oracledb.autoCommit = true

const connConf = {
	user: 'pm',
	password: '123456',
	connectString: 'localhost:1521/xe'
}

cloudinary.config({
	cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
	api_key: process.env.CLOUDINARY_API_KEY,
	api_secret: process.env.CLOUDINARY_API_SECRET,
})

const fs = require('fs-extra')

function resParse(res) {
	const result = []
	res.rows.forEach(element => {
		let obj = {}
		element.forEach((value, index) => {
			obj[`${res.metaData[index].name.toLowerCase()}`] = value
		})
		result.push(obj)
	})
	return result
}

const getProjects = async (req, res) => {
	const connection = await oracledb.getConnection(connConf)
	console.log('DB is connected')
	const response = await connection.execute(
		'SELECT * FROM proyecto'
	)
	await connection.close()
	res.json(resParse(response))
}

const getRoute = async (req, res) => {
	const connection = await oracledb.getConnection(connConf)
	const { id } = req.params
	const response = await connection.execute(
		`
		SELECT * FROM ruta
		WHERE CodPyto = ${id}
		`
	)
	await connection.close()
	res.json(resParse(response))
}

const getTramos = async (req, res) => {
	const connection = await oracledb.getConnection(connConf)
	const { id } = req.params
	const response = await connection.execute(
		`
		SELECT * FROM tramo
		WHERE CodRuta = ${id}
		ORDER BY CodTramo
		`
	)
	await connection.close()
	res.json(resParse(response))
}

const postTramo = async (req, res) => {
	console.log(req.file)
	console.log(req.params)
	console.log(req.body)
	const { id } = req.params
	const {
		codruta,
		vigencia,
		duracion,
		proginicio,
		latinicio,
		longinicio,
		altinicio,
		progfinal,
		latfin,
		longfin,
		altfin,
		idcriterioini,
		dencorta,
		observac
	} = req.body
	try {
		const connection = await oracledb.getConnection(connConf)
		const result = await cloudinary.v2.uploader.upload(
			req.file.path,
			{
				resource_type: 'video',
				public_id: `bd-project/${req.file.filename.replace('.mp4','')}`
			}
		)
		console.log(result)

		const response = await connection.execute(
			`
			insert into tramo(
				CodTramo, CodRuta, CodTramoPy, Vigencia,
				Duracion, ProgInicio, LatInicio, LongInicio,
				AltInicio, ProgFinal, LatFin, LongFin, AltFin, IdCriterioIni,
				DenCorta, Observac, VideoUrl, VideoId
			) values(
				tramo_seq.NEXTVAL,
				:codruta,
				'${id}/' || to_char(tramo_seq.NEXTVAL, 'FM000'),
				:vigencia,
				:duracion,
				:proginicio,
				:latinicio,
				:longinicio,
				:altinicio,
				:progfinal,
				:latfin,
				:longfin,
				:altfin,
				:idcriterioini,
				:dencorta,
				:observac,
				:videourl,
				:videoid
			)
			`,
			{
				codruta: Number(codruta),
				vigencia,
				duracion: Number(duracion),
				proginicio,
				latinicio,
				longinicio,
				altinicio,
				progfinal,
				latfin,
				longfin,
				altfin,
				idcriterioini,
				dencorta,
				observac,
				videourl: result.secure_url,
				videoid: result.public_id
			}
		)
		console.log(response)

		await connection.close()
		await fs.unlink(req.file.path)
		res.json({})
	} catch(e) {
		if (await fs.pathExists(req.file.path)) await fs.unlink(req.file.path)
		console.log('video eliminado')
		console.log(e)
		res.json({error: e})
	}
}

const putTramo = async (req, res) => {
	const { id } = req.params
	const {
		vigencia,
		duracion,
		proginicio,
		latinicio,
		longinicio,
		altinicio,
		progfinal,
		latfin,
		longfin,
		altfin,
		idcriterioini,
		dencorta,
		observac,
		videoid
	} = req.body

	console.log(req.params)
	console.log(req.body)
	console.log(req.file)

	try {
		const connection = await oracledb.getConnection(connConf)
		if(req.file){
			await cloudinary.v2.api.delete_resources(
				[`${videoid}`],
				{
					resource_type: 'video'
				}
			)

			const result = await cloudinary.v2.uploader.upload(
				req.file.path,
				{
					resource_type: 'video',
					public_id: `bd-project/${req.file.filename.replace('.mp4','')}`
				}
			)
			const response = await connection.execute(
				`
				update tramo
				set Vigencia = :vigencia,
					Duracion = :duracion,
					ProgInicio = :proginicio,
					LatInicio = :latinicio,
					LongInicio = :longinicio,
					AltInicio = :altinicio,
					ProgFinal = :progfinal,
					LatFin = :latfin,
					LongFin = :longfin,
					AltFin = :altfin,
					IdCriterioIni = :idcriterioini,
					DenCorta = :dencorta,
					Observac = :observac,
					VideoUrl = :videourl,
					VideoId = :videoid
				where CodTramo = :codtramo 
				`,
				{
					vigencia,
					duracion: Number(duracion),
					proginicio,
					latinicio,
					longinicio,
					altinicio,
					progfinal,
					latfin,
					longfin,
					altfin,
					idcriterioini,
					dencorta,
					observac,
					videourl: result.secure_url,
					videoid: result.public_id,
					codtramo: Number(id)
				}
			)
			await connection.close()
			await fs.unlink(req.file.path)
		} else {
			console.log(`actualizacion sin video ${id}`)
			const response = await connection.execute(
				`
				update tramo
				set Vigencia = :vigencia,
					ProgInicio = :proginicio,
					LatInicio = :latinicio,
					LongInicio = :longinicio,
					AltInicio = :altinicio,
					ProgFinal = :progfinal,
					LatFin = :latfin,
					LongFin = :longfin,
					AltFin = :altfin,
					IdCriterioIni = :idcriterioini,
					DenCorta = :dencorta,
					Observac = :observac
				where CodTramo = :codtramo 
				`,
				{
					vigencia,
					proginicio,
					latinicio,
					longinicio,
					altinicio,
					progfinal,
					latfin,
					longfin,
					altfin,
					idcriterioini,
					dencorta,
					observac,
					codtramo: Number(id)
				}
			)
			console.log('hola amigos')
			await connection.close()
		}
		res.json(null)
	} catch (e) {
		if (await fs.pathExists(req.file)) await fs.unlink(req.file.path)
		res.json({ error: e })
	}
}

const deleteTramo = async (req, res) => {
	console.log(req.params)
	console.log(req.query)
	const { id } = req.params
	const { videoid } = req.query

	const connection = await oracledb.getConnection(connConf)

	await cloudinary.v2.api.delete_resources(
		[`${videoid}`],
		{
			resource_type: 'video'
		}
	)
	console.log('video eliminado')

	const response = await connection.execute(
		`
		delete from tramo
		where CodTramo = ${id}
		`
	)
	await connection.close()
	res.json(null)
}


module.exports = {
	getProjects,
	getRoute,
	getTramos,
	postTramo,
	putTramo,
	deleteTramo
}
