
create sequence prueba_seq
	increment by 1
	start with 1
	maxvalue 100
	nocache
	nocycle;


create table prueba (
	dni number(6),
	nombre varchar2(20),
	edad number(2),
	constraint pk_prueba primary key (dni)
);

insert into prueba(dni, nombre, edad)
values(prueba_seq.NEXTVAL, 'Eric', 26);

select * from prueba;
select * from tramo;
select * from employees;


const response = await connection.execute(
	`
	insert into prueba(
		dni,
		nombre,
		edad
	) values(
		prueba_seq.NEXTVAL,
		:nombre,
		:edad
	)
	`,
	{
		nombre: 'Kari',
		edad: 18
	},
	{ autoCommit: true }
)
