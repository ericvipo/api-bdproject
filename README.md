//Config VIM
let g:dbext_default_profile_ORA_HR = 'type=ORA:srvname=//localhost\:1521/xe:user=hr:passwd=hr'
let g:dbext_default_profile_ORA_ERIC = 'type=ORA:srvname=//localhost\:1521/xe:user=eric:passwd=123456'
let g:dbext_default_profile_ORA_PM = 'type=ORA:srvname=//localhost\:1521/xe:user=pm:passwd=123456'

//Enviroment variables => .zshrc
export LD_LIBRARY_PATH='/u01/app/oracle/product/11.2.0/xe/lib'
export CLOUDINARY_CLOUD_NAME='XXXXXX'
export CLOUDINARY_API_KEY='XXXXXX'
export CLOUDINARY_API_SECRET='XXXXXX'

. /u01/app/oracle/product/11.2.0/xe/bin/oracle_env.sh