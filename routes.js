const { Router } = require('express')
const router = Router()

const {
	getProjects,
	getRoute,
	getTramos,
	postTramo,
	putTramo,
	deleteTramo
} = require('./controller')

router.get('/projects', getProjects)

router.get('/route/:id', getRoute)

router.get('/tramos/:id', getTramos)
router.post('/tramo/:id', postTramo)
router.put('/tramo/:id', putTramo)
router.delete('/tramo/:id', deleteTramo)

module.exports = router
